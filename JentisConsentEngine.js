/**
 * The JENTIS consent engine 1.0.17
 *
 * The JENTIS consent engine is the central API component
 * on page to communicate with all other consent components.
 *
 */
window.jentis = window.jentis || {};
window.jentis.consent = window.jentis.consent || {};

window.jentis.consent.engine = new function () {
    //*************************
    //*************************
    //INIT FUNCTION
    //*************************
    //*************************

    /**
     * While Loading we want to init all the consent and vendor status.
     */
    this.init = function () {

        this.v = "2";

        //Because of ID changens at version 2 of JENTIS, we have to migrate old vendorIds.
        this.setV2MappingMigration();

        //Set Fallback Configurations.
        this.setFallbackConfiguration();

        //Global variables
        this.aEventCache = {};				//Event Cache for async Callback
        this.bNewVendorConsent = false;			//Bool, gibt an ob in der Congig Tools gefunden wurden die noch nicht im Storage sind.

        this.sConsentId = false;			//Current ConsentID
        this.bUserConsent = false;			//If true, the consent was given from the user, if false, the consent was given without the user (consent mode and bModeStartInitTrackOnNoConsentMode)
        this.iLastUpdate = false;			//Date in Unixtimestamp when the last save happend.
        this.bSend = false;			        //true if the consent is send, false if never before send. Important for migration.
        this.aStorage = {};				    //List of bools for each pixel from the loca storage
        this.vendorsConsentModeFromStorage = {};
        this.aInitStorage = {};				//List of bools for each pixel (Status since the last save action)

        this.bStartTrack = null;				//Current Status if allready started to track.
        this.consentData = {};

        //Check if we are within a iframe
        this.bIframe = window.self !== window.top;

        //Start with init processing
        this.readStorage();
        this.init_eventlistener();
        this.init_consentStatus();
        var bBarShow = this.checkifBarShow();
        this.startJentisTracking(bBarShow);
    }


    //*************************
    //*************************
    //MIGRATION FUNCTIONS
    //*************************
    //*************************


    this.setV2MappingMigration = function () {
        if (typeof window.jentis.consent.v2mapper !== "undefined") {
            this.vendorV2Migration = window.jentis.consent.v2mapper;
        } else {
            this.vendorV2Migration = {
                "adformDMP": "adformdmp",
                "ga": "googleanalytics",
                "fb": "facebook",
                "partnerizeservertoserver": "partnerize",
                "googleoptimize": "googleopt",
                "cjaffiliate": "cj-affiliate",
                "scrolltracker": "scrolltracker"
            };
        }
    }

    this.setFallbackConfiguration = function () {
        if (typeof window.jentis.consent.config !== "undefined") {

            if (Object.keys(window.jentis.consent.config.vendors).length === 0) {
                window.jentis.consent.config.vendors = {
                    "*": {
                        "category" : {
                          "id" : "fallback"
                        },
                        "vendor": {
                            "id": "*",
                            "name": "",
                            "street": "",
                            "zip": "",
                            "country": {
                                "iso": "-",
                                "name": ""
                            }
                        },
                        "purpose": {
                            "id": "other",
                            "name": "Other"
                        },
                        "justification": {
                            "id": "other",
                            "name": "other"
                        },
                        "consentMode": 1,
                        "description": ""
                    }
                };
            }

            this.oLocalConfData = window.jentis.consent.config;
        } else {
            console.log("jentis.consent.engine config not found - fallback config");
            this.oLocalConfData = {
                timeoutBarShow: -1,
                backward: {},
                bModeStartInitTrackOnNoConsentMode: true,
                template: {},
                vendors:
                    {
                        "*": {
                            "category" : {
                                "id" : "fallback"
                            },
                            "vendor": {
                                "id": "*",
                                "name": "",
                                "street": "",
                                "zip": "",
                                "country": {
                                    "iso": "-",
                                    "name": ""
                                }
                            },
                            "purpose": {
                                "id": "other",
                                "name": "Other"
                            },
                            "justification": {
                                "id": "other",
                                "name": "other"
                            },
                            "consentMode": 1,
                            "description": ""
                        }
                    }
            }
        }
    }


    //*************************
    //*************************
    //HELPER FUNCTION FOR INIT
    //*************************
    //*************************

    /**
     * Checks if we want to start tracking. If the consentbar is currently showen, then we do not start tracking.
     * If we have at least one positive consent, then we start tracking.
     *
     *@param bool bBarShow Pass if the consentbar is currently showen.
     */
    this.startJentisTracking = function (bBarShow) {
        var bTrack = false;
        for (var sVendorId in this.oLocalConfData.vendors) {
            var oVendorConfig = this.oLocalConfData.vendors[sVendorId];

            if (oVendorConfig.consentMode === 0 || oVendorConfig.consentMode === 2) {
                //If consent mode is 0 (consent) or 2 (essential mode) check if the given consent is true or ncm - ncm can be only for consent mode 2, then start tracking
                if (
                    typeof this.sConsentId !== "undefined" &&
                    this.sConsentId !== false &&
                    bBarShow === false &&
                    (
                        this.aStorage[sVendorId] === true || 
                        this.aStorage[sVendorId] === "ncm"
                    )
                ) {
                    bTrack = true;
                    break;
                }
            } else if(oVendorConfig.consentMode === 1) {
                //if consent mode = 1: No consent mode, so we can track regardless
                bTrack = true;
                break;
            }
        }

        if (bTrack === true && this.bStartTrack !== true) {
            window.jentis.helper.setEvent("jentis.consent.engine", "minimal-consent-given", null, true);
        } else if (bTrack === false && this.bStartTrack !== false) {
            window.jentis.helper.setEvent("jentis.consent.engine", "no-consent-given", null, true);
        }

        this.bStartTrack = bTrack;
    }


    /**
     * Organize the inital status of each vendor.
     */
    this.init_consentStatus = function () {
        if (this.sConsentId === false) {
            //Noch kein Storage gesetzt, daher aus Config initial setzen.
            var aStorage = {};
        } else {
            var aStorage = this.aStorage;
        }

        this.bNewVendorConsent = false;
        this.bWriteStorage = false;

        /**
         * Iterate through the vendors configured in the configuration.
         * If there would be a vendor added, changed or removed, we have to investigate
         * these different situations in the init process.
         */
        var bNoConsentMode = false;
        for (var sVendorId in this.oLocalConfData.vendors) {
            var oVendorConfig = this.oLocalConfData.vendors[sVendorId];

            if (typeof aStorage[sVendorId] === "undefined") {
                /**
                 *  We do not have a stored consent of this vendor!
                 *  Check: If from justification "consent" we have to ask for the consent.
                 *  Else: Every other justification leads to directly start tracking.
                 */
                if (oVendorConfig.consentMode === 0 || oVendorConfig.consentMode === 2) {
                    //Don't set the Consent automatically to false, because there is no decission made if true/ncm/false
                    //Now we have to know that there is a need for a new consent.
                    this.bNewVendorConsent = true;
                } else if (oVendorConfig.consentMode === 1 ) {
                    //if the justification is NOT consent, we can start to track.
                    aStorage[sVendorId] = true;
                    bNoConsentMode = true;
                }

                this.bWriteStorage = true;
            } else {
                /**
                 *  We do have a stored consent-information for the given vendor.
                 *  We have to check if the justification changed and handle the different situations
                 */
                if(typeof this.vendorsConsentModeFromStorage[sVendorId] !== "undefined") {

                    if(this.vendorsConsentModeFromStorage[sVendorId] !== oVendorConfig.consentMode) {
                        if(oVendorConfig.consentMode === 0 || oVendorConfig.consentMode === 2) {
                            //Don't set the Consent automatically to false, because there is no decission made if true/ncm/false
                            //Now we have to know that there is a need for a new consent.
                            // But we don't change the consent, because it will be coming from the connected bridge
                            this.bNewVendorConsent = true;
                        } else if (oVendorConfig.consentMode === 1 ) {
                            //if the justification is NOT consent, we can start to track.
                            aStorage[sVendorId] = true;
                            bNoConsentMode = true;
                        }

                        this.bWriteStorage = true;
                    }

                }else{

                    // Fallback: New justificationJSON is not existing
                    // Do not change something on justification "consent"
                    if( oVendorConfig.consentMode === 1) {
                        //if the justification is NOT consent, we can start to track.
                        aStorage[sVendorId] = true;
                        bNoConsentMode = true;
                        this.bWriteStorage = true;
                    }
                }
            }
        }

        //Check if * vendor and specific vendors are part of sStorage. If so: delete the * vendor
        if (Object.keys(aStorage).length > 1 && typeof aStorage["*"] !== "undefined") {
            delete aStorage["*"];
            this.bWriteStorage = true;
        }


        //Initial we have to write the storage situation, because at init no change event should fired.
        this.aInitStorage = this.copyObject(aStorage);


        var bSendConsent = false;
        var bFromUser = false;
        //If there is a consent storage (consentid exists) and we've never sent the consent before, now we have to send it.
        if (this.bSend === false && this.sConsentId !== false) {
            bSendConsent = true;
            bFromUser = true;
        }

        //If we've never sent the consent before AND there was at least one vendor with a justification not Consent AND in the config we want to start tracking initial
        //when a no consent vendor was configured.
        if (this.bSend === false && bNoConsentMode === true && this.oLocalConfData.bModeStartInitTrackOnNoConsentMode === true) {
            bSendConsent = true;
            bFromUser = false;
        }

        //If there is a consent storage (consentid exists), and we've sent the consent before, and there is a new vendor.
        if (this.bSend === true && this.sConsentId !== false && this.bNewVendorConsent === true) {
            bSendConsent = true;
            bFromUser = true;
        }

        if (this.bWriteStorage) {
            this.writeStorage(aStorage, bSendConsent, true, false, bFromUser);
        } else if (bSendConsent) {
            //If there is a consent storage (consentid exists) and we've never sent the consent before, now we have to send it.
            //If we have to send it even without a change, we must go to writeStorage.
            this.writeStorage(aStorage, bSendConsent, false, false, bFromUser);
        }

        window.jentis.helper.setEvent("jentis.consent.engine", "init", null, true);

    }

    /**
     * Check if the consentbar should shown to the user or not.
     *
     *@return bool true if the consentbar should be showen, false if nothing should be showen to the user.
     */
    this.checkifBarShow = function () {
        if (Object.keys(this.aStorage).length === 0 || this.sConsentId === false) {
            //No consent is stored
            window.jentis.helper.setEvent("jentis.consent.engine", "show-bar", null, true);
            return true;
        } else {
            if (this.bUserConsent === false) {
                //The given consent was not set by the user, so show the consentbar again.
                window.jentis.helper.setEvent("jentis.consent.engine", "show-bar", null, true);
                return true;
            }

            if (this.bNewVendorConsent === true) {
                //At least one new tool is added to the vendor consent list, so ask for consent again.
                window.jentis.helper.setEvent("jentis.consent.engine", "show-bar", null, true);
                return true;
            }

            if (this.iLastUpdate === false || (typeof this.oLocalConfData.timeoutBarShow !== "undefined" && this.oLocalConfData.timeoutBarShow !== false && this.iLastUpdate + this.oLocalConfData.timeoutBarShow < Date.now())) {
                //Max Time of consent storage is over, so we have to ask again.
                window.jentis.helper.setEvent("jentis.consent.engine", "show-bar", null, true);
                return true;

            } else {
                //No timeout and no new consent based vendor, so do not show the consentbar.
                return false;
            }
        }
    }


    /**
     * Read the storage from the localStorage and write it to the variables.
     *
     */
    this.readStorage = function () {
        //Get the data from the local storage.
        var aData = null;
        var cookiedata = window.jentis.helper.readCookie("jts_cmp");
        if (window.jentis.helper.bIsLocalStorageAvailable === true) {
            if (cookiedata !== null) {
                // If a Cookie is existing, store it into the storage
                aData = JSON.parse(cookiedata);

                window.jentis.helper.setCookie({
                    "name": "jts_cmp",
                    "value": "",
                    "exdays": -1,
                    "sameSite": "Strict"
                });
            } else {
                aData = window.jentis.helper.getLocalStorageItem("jentis.consent.data");
            }
        } else {
            if (cookiedata !== null) {
                aData = JSON.parse(cookiedata);
            }
        }

        if (aData === null) {
            //If not consent is stored.
            this.sConsentId = false;
            this.iLastUpdate = false;
            this.bSend = false;

            //Now we want to set the initial consent to false is the justification is consent, if we have an otherwise
            //justification then consent then the inital status is true.
            this.aStorage = {};
            for (var sVendorId in this.oLocalConfData.vendors) {
                var oVendorData = this.oLocalConfData.vendors[sVendorId];
                if (oVendorData.consentMode === 1) {
                    this.aStorage[sVendorId] = true;
                } else {
                    this.aStorage[sVendorId] = false;
                }
            }

            //Set the initial storage to empty object to realize the different when we want to store the status.
            this.aInitStorage = {};
        } else {
            this.sConsentId = aData.consentid;
            this.iLastUpdate = aData.lastupdate;
            this.aStorage = aData.vendors;
            this.bUserConsent = aData.userconsent;
            this.consentData = aData.data;

            // Backwards Compatible [New: justification per vendor stored]
            if(typeof aData.vendorsJustification !== "undefined") {
                for (var vendorz in aData.vendorsJustification){
                    if(typeof aData.vendorsJustification[vendorz] !== "undefined" && aData.vendorsJustification[vendorz] !== "consent"){
                        this.vendorsConsentModeFromStorage[vendorz] = 1;
                    }else{
                        this.vendorsConsentModeFromStorage[vendorz] = -1; // Consent cannot be translated into consentMode, this point it can be 0(normal) or 2(ncm), have to chaek later
                    }
                }
            }

            if(typeof aData.vendorsConsentMode !== "undefined") {
                this.vendorsConsentModeFromStorage = aData.vendorsConsentMode;
            }

            // Backwards compatible [Change of the internal JSON structure]
            if (typeof this.aStorage === "undefined" &&
                typeof this.oLocalConfData.backward !== "undefined" &&
                typeof this.oLocalConfData.backward.vendorduplicate !== "undefined"
            ) {
                this.aStorage = aData[this.oLocalConfData.backward.vendorduplicate];
            }

            //V2 Migration of old vendorIDs.
            var bV2Migration = false;
            for (var sOldVendorId in this.aStorage) {
                var sNewVendorId = this.vendorV2Migration[sOldVendorId];
                if (typeof sNewVendorId !== "undefined") {
                    this.aStorage[sNewVendorId] = this.aStorage[sOldVendorId];
                    delete this.aStorage[sOldVendorId];
                    bV2Migration = true;
                }
            }

            //If there is a new storage because of Migration, then strore it now even to the local storage.
            if (bV2Migration) {
                aData.vendors = this.aStorage;

                //Backwards compatible
                if (typeof this.aStorage === "undefined" &&
                    typeof this.oLocalConfData.backward !== "undefined" &&
                    typeof this.oLocalConfData.backward.vendorduplicate !== "undefined"
                ) {
                    aData[this.oLocalConfData.backward.vendorduplicate] = this.aStorage;
                }

                this.store2LocalStorage(aData);
            }
            //END V2 Migration of old vendorIDs.


            this.aInitStorage = this.copyObject(aData.vendors);

            //If there is a storage previously by migration not set by JENTIS Consent Engine, then the send variables
            //is not existing. Then we want to send the consent again to the JENTIS Server.
            if (typeof aData.send !== "undefined") {
                this.bSend = aData.send;
            } else {
                this.bSend = false;
            }

        }

    }


    //*************************
    //*************************
    //PUBLIC GET FUNCTION
    //*************************
    //*************************


    /**
     * Returns the configuration for the used consent bar template
     *
     *@return object The config Object.
     */
    this.getTemplateConfig = function () {
        if (typeof this.oLocalConfData !== "undefined") {
            return this.oLocalConfData;
        } else {
            return false;
        }
    }


    /**
     * Returns the consent status of a passed vendor
     *
     *@param string sVendorId The Id of the vendor which status you want to know.
     *@return bool Consent Status of the vendor.
     */
    this.getVendorConsent = function (sVendorId) {
        return this.aStorage[sVendorId];
    }

    /**
     * Return Consent Status of all vendors.
     *
     *@return object Returns an object with all vendors as keys and per vendor true or fals whether the consent is set or not.
     */
    this.getAllConsents = function () {
        return this.aStorage;
    }

    /**
     * Return the current consentid
     *
     *@return string The current ConsentId or false if no one was created so far.
     */
    this.getConsentId = function () {
        return this.sConsentId;
    }

    /**
     * Returns the full data of the vendors from the config.
     *
     *@return object The VendorIds as keys and an object with data about the vendor per vendorId
     */
    this.getVendorFullData = function () {
        var oVendors = {};
        for (var sVendorId in this.oLocalConfData.vendors) {
            oVendors[sVendorId] = this.oLocalConfData.vendors[sVendorId];
            oVendors[sVendorId].status = this.aStorage[sVendorId];
        }

        return oVendors;
    }

    /**
     * Returns the Unix Timestamp in microseconds of the last update of the user.
     *
     *@return integer The last Update Time in Unix Timestamp microseconds.
     */
    this.getLastUpdateTime = function () {
        return this.iLastUpdate;
    }

    /**
     * This function retrieves vendors that have a specific Google consent type configured in DCP.
     *
     * @param {string} strGoogleConsentType - The Google consent type to check for.
     * @returns {Array} arrVendorsWithConsentType - An array of vendors that have the specified Google consent type.
     */
    this.getVendorsOfGCT = function(strGoogleConsentType){
        var strConsentType = strGoogleConsentType;
        var arrVendorsWithConsentType = [];

        if (
            typeof window.jentis.consent.config !== 'undefined' &&
            typeof window.jentis.consent.config.vendors !== 'undefined' &&
            Object.keys(window.jentis.consent.config.vendors).length > 0
        ){
            var objVendors = window.jentis.consent.config.vendors;

            for(var vendor in objVendors){
                if (
                    typeof objVendors[vendor].googleConsentMode !== 'undefined' &&
                    typeof objVendors[vendor].googleConsentMode[strConsentType] !== 'undefined' &&
                    objVendors[vendor].googleConsentMode[strConsentType] === true
                ){
                    arrVendorsWithConsentType.push(vendor);
                }
            }
        }

        return arrVendorsWithConsentType;
    }

    /**
     * This function checks if there is any vendor that has the specified Google consent type and has given consent.
     *
     * @param {string} strGoogleConsentType - The Google consent type to check for.
     * @returns {boolean|null} - Returns true if any vendor with the specified Google consent type has given consent, false if none of them have given consent, and null if there are no vendors with the specified Google consent type.
     */
    this.getConsentOfGCT = function(strGoogleConsentType){
        var arrConfiguredVendors = this.getVendorsOfGCT(strGoogleConsentType);
    
        if (arrConfiguredVendors.length > 0){
            var bConsent = false;

            for(var vendorsCnt=0; vendorsCnt < arrConfiguredVendors.length; vendorsCnt++){
                var bVendorConsent = this.getVendorConsent(arrConfiguredVendors[vendorsCnt]);

                if( bVendorConsent === true ){
                    bConsent = true;
                    break;
                }
            }
            return bConsent;
        }

        return null;

    }


    //*************************
    //*************************
    //PUBLIC SET FUNCTION
    //*************************
    //*************************

    this.setNewVendorConsentsIgnoreConsentMode = function(aVendorConsents) {
        this.setNewVendorConsents(aVendorConsents, true);
    }

    /**
     * Set new status of vendors.
     *
     *@param array aVendorConsents The new status auf the vendors. The vendorId is the key and the value is bool true or false.
     */
    this.setNewVendorConsents = function (aVendorConsents, ignoreConsentModeSettings) {

        // Defult Value is false! We want to stick to the consentmode settings
        if(typeof ignoreConsentModeSettings === "undefined") {
            ignoreConsentModeSettings = false;
        }

        //We want to override those vendors which are defined by the parameter.
        for (var sVendorId in aVendorConsents) {
            this.aStorage[sVendorId] = aVendorConsents[sVendorId];

            if(!ignoreConsentModeSettings) {
                //If this vendor is enabled for no consent mode and if the consent is false, then set the consent to "ncm"
                if (
                    this.aStorage[sVendorId] === false &&
                    typeof this.oLocalConfData.vendors[sVendorId] !== "undefined" &&
                    typeof this.oLocalConfData.vendors[sVendorId]["consentMode"] !== "undefined" &&
                    this.oLocalConfData.vendors[sVendorId]["consentMode"] === 2
                ) {
                    this.aStorage[sVendorId] = "ncm";
                }
                
                if (
                    this.aStorage[sVendorId] !== true &&
                    typeof this.oLocalConfData.vendors[sVendorId] !== "undefined" &&
                    typeof this.oLocalConfData.vendors[sVendorId]["consentMode"] !== "undefined" &&
                    this.oLocalConfData.vendors[sVendorId]["consentMode"] === 1
                ) {
                    this.aStorage[sVendorId] = true;
                }
            }
        }

        //Now set the new storage to the localstorage
        return this.writeStorage(this.aStorage, true);
    }

    /**
     * Opt out, means really all vendors to false, even if they have legitimate interest or no consent mode activated
     */
    this.OptOut = function () {

        //Set all vendors to false if justification is consent, otherwise it must be set to true
        var aStorage = {};
        for (var sVendorId in this.oLocalConfData.vendors) {
            aStorage[sVendorId] = false;
        }

        //Now set the new storage to the localstorage
        return this.writeStorage(aStorage, true);
    }


    /**
     * Denies all consents of all vendors.
     */
    this.DenyAll = function () {

        //Set all vendors to false if justification is consent, otherwise it must be set to true
        var aStorage = {};
        for (var sVendorId in this.oLocalConfData.vendors) {
            var oVendorData = this.oLocalConfData.vendors[sVendorId];

            if ( oVendorData.consentMode === 0 ) {

                aStorage[sVendorId] = false;

            } else if ( oVendorData.consentMode === 2 ){
                //If this vendor's consentMode is 3(essential mode) and if the consent is false, then set the consent to "ncm"
                aStorage[sVendorId] = "ncm";

            } else if ( oVendorData.consentMode === 1 ) {

                aStorage[sVendorId] = true;

            }

        }

        //Now set the new storage to the localstorage
        return this.writeStorage(aStorage, true);
    }

    /**
     * All vendors get a positiv consent.
     */
    this.AcceptAll = function () {
        //Set all vendors to true
        var aStorage = {};
        for (var sVendorId in this.oLocalConfData.vendors) {
            aStorage[sVendorId] = true;
        }

        //Now set the new storage to the localstorage
        return this.writeStorage(aStorage, true);
    }

    /**
     * The user requestst to get the setting panel in order to change is settings.
     */
    this.userShowSettings = function () {
        //Just throw the event so others can show the setting panel.
        window.jentis.helper.setEvent("jentis.consent.engine", "user-show-settings", null, true);
    }


    //*************************
    //*************************
    //INTERNAL EVENT  FUNCTION
    //*************************
    //*************************


    /**
     * External can register their event with this function. If they missed the events, we can call their callback immidiatly.
     *
     *@param string sName The name of the event to register.
     *@param function cb The callback which should be called when the event is called.
     */
    this.addEventListener = function (sName, cb) {
        window.jentis.helper.addEventListener(sName, cb, true);
    }

    /**
     *DEPRECATED: Starts listen to different events
     *
     *@deprecated No longer events are used, but still supported in order to be backwars compatible.
     */
    this.init_eventlistener = function () {
        //Trigger for external to set vendors consents.
        (function (oMe) {
            document.addEventListener('jentis.consent.engine.setNewVendorConsents', function (e) {
                oMe.setNewVendorConsents(e.details.vendors);
                window.jentis.helper.setEvent("jentis.consent.engine", "external-NewVendorData", null, true);


            }, false);

        })(this);


        //Trigger for external to deny all.
        (function (oMe) {
            document.addEventListener('jentis.consent.engine.DenyAll', function (e) {
                oMe.alldeny();
                window.jentis.helper.setEvent("jentis.consent.engine", "external-DenyAll", null, true);

            }, false);

        })(this);

        //Trigger for external to accept all.
        (function (oMe) {
            document.addEventListener('jentis.consent.engine.AcceptAll', function (e) {
                oMe.allagree();
                window.jentis.helper.setEvent("jentis.consent.engine", "external-AcceptAll", null, true);

            }, false);

        })(this);


    }


    //*************************
    //*************************
    //INTERNAL BASIC FUNCTIONS
    //*************************
    //*************************

    /**
     * Check ob sich am Storage etwas geändert hat
     *
     *@param object oData2Check The vendor consent data to be checked against the local storage.
     *@return bool If true, something has changed, if false nothing has changed.
     */
    this.checkStorageChange = function (oData2Check) {

        var aPosChange = [];
        var aPosNegChange = {};
        var bChange = false;
        var bShouldWeSendTheConsentDoc = false;

        for (var sKey in oData2Check) {
            if (

                ((typeof this.aInitStorage[sKey] === "undefined" || this.aInitStorage[sKey] === false) && oData2Check[sKey] === true) ||
                ((typeof this.aInitStorage[sKey] !== "undefined" && this.aInitStorage[sKey] === "ncm") && oData2Check[sKey] === true) ||
                ((typeof this.aInitStorage[sKey] === "undefined" || this.aInitStorage[sKey] === false) && oData2Check[sKey] === "ncm")

            ) {
                //This Consent was added
                aPosChange.push(sKey);
                aPosNegChange[sKey] = oData2Check[sKey];
                bChange = true;
                bShouldWeSendTheConsentDoc = true;
            } else if (

                (typeof this.aInitStorage[sKey] !== "undefined" && this.aInitStorage[sKey] === true && oData2Check[sKey] === false) ||
                (typeof this.aInitStorage[sKey] !== "undefined" && this.aInitStorage[sKey] === true && oData2Check[sKey] === "ncm") ||
                (typeof this.aInitStorage[sKey] !== "undefined" && this.aInitStorage[sKey] === "ncm" && oData2Check[sKey] === false)

            ) {
                //This Consent was deleted
                bChange = true;
                aPosNegChange[sKey] = oData2Check[sKey];
                bShouldWeSendTheConsentDoc = true;
            }
        }

        if (aPosChange.length > 0) {
            //There are positive consent changes, now send the event
            window.jentis.helper.setEvent("jentis.consent.engine", "vendor-add", aPosChange, true);

        }

        if (bChange === true) {
            //There are consent changes, so now send the regarding event.
            window.jentis.helper.setEvent("jentis.consent.engine", "vendor-change", oData2Check, true);
        }


        //Now we are ready with the comparison, so prepare for the next comparison
        this.aInitStorage = this.copyObject(oData2Check);
        return [aPosNegChange, bShouldWeSendTheConsentDoc];

    }

    /**
     * Writes the current vendor consent data to the local storage and optional send it to the JENTIS Tracker
     *
     *@param array aStorage The current vendor consent data to store.
     *@param bool bSend If true we will send the current consent data to the JENTIS Tracker
     *@param bool bRenewTimestamp If true we will create a new current timestamp and add it to the storage data
     *@param bool bstartTrack If true we will check if we must start tracking.
     */
    this.writeStorage = function (aStorage, bSend, bRenewTimestamp, bStartTracking, bFromUser) {
        if (typeof bFromUser === "undefined") {
            bFromUser = true;
        }
        this.bUserConsent = bFromUser;

        //We just want to set a consentId if we are sending it to the server.
        if (this.sConsentId === false && bSend === true) {
            this.sConsentId = this.uuidv4();
        }

        //If bSend not passed, then allways send it.
        if (typeof bSend === "undefined") {
            bSend = true;
        }

        //If bStartTracking not passed, then allways start Tracking
        if (typeof bStartTracking === "undefined") {
            bStartTracking = true;
        }

        //We are storing the update time no matter if we are sending or not.
        if (typeof bRenewTimestamp === "undefined" || bRenewTimestamp === true) {
            this.iLastUpdate = Date.now();
        }

        //Create the data to store
        var aData = {
            consentid: this.sConsentId,
            lastupdate: this.iLastUpdate,
            vendors: aStorage,
            vendorsConsentMode : this.getConsentModePerVendor(),
            send: bSend,
            userconsent: bFromUser,
            data : this.consentData
        };

        //Backwards compatible
        if (
            typeof this.oLocalConfData.backward !== "undefined" &&
            typeof this.oLocalConfData.backward.vendorduplicate !== "undefined"
        ) {
            aData[this.oLocalConfData.backward.vendorduplicate] = aStorage;
        }

        //Now write it to the local storage
        this.store2LocalStorage(aData);

        //We want to have the new storage data even in the object storage variables
        this.aStorage = aStorage;

        //Check if something had changed so we can trigger the events.
        var aChangeResult = this.checkStorageChange(aStorage);
        var oVendorsChanged = aChangeResult[0];
        var bNewConsentDocSendBecauseOfChange = aChangeResult[1];

        aData["vendorsChanged"] = oVendorsChanged;

        //Now we want to send it if wanted
        if (bSend === true && (bNewConsentDocSendBecauseOfChange === true || this.bSend === false)) {
            window.jentis.helper.setEvent("jentis.consent.engine", "send-consent-data", aData, true);
            //We can only set it to true. If send not wanted, may it is allready send to bSend is correctly mayba true.
            this.bSend = true;
        }


        //Now we want to check if we want to start to track.
        if (bStartTracking === true) {
            this.startJentisTracking(false);
        }

        //Return the consentID
        return this.sConsentId;
    }


    this.store2LocalStorage = function (aData) {
        if (window.jentis.helper.bIsLocalStorageAvailable === true) {
            window.jentis.helper.setLocalStorageItem("jentis.consent.data", aData);
        } else {
            window.jentis.helper.setCookie({
                "name": "jts_cmp",
                "value": JSON.stringify(aData),
                "exdays": 365,
                "sameSite": "Strict"
            });
        }
    }

    /**
     * Return a JSON consisting vendorid as key and consent mode as integer 0/1/2
     *
     * @returns {{}}
     */
    this.getConsentModePerVendor = function() {
        var vendorConsentModeJSON = {};

        for(var vendor in this.oLocalConfData.vendors) {
            if(
                typeof this.oLocalConfData.vendors[vendor].consentMode !== "undefined" &&
                this.oLocalConfData.vendors[vendor].consentMode !== null
            ) {
                vendorConsentModeJSON[vendor] = this.oLocalConfData.vendors[vendor].consentMode;
            }
        }

        return vendorConsentModeJSON;
    }

    /**
     * Return a GUID in Version 4
     *
     *@return string The GUID String in Version 4
     *
     */
    this.uuidv4 = function () {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }

    /**
     * Copy a object not by reference
     *
     *@param object oObj The object which must be copied.
     *@return string The new copy of the object.
     *
     */
    this.copyObject = function (oObj) {
        var oNewObj = {};
        for (var sKey in oObj) {
            oNewObj[sKey] = oObj[sKey];
        }
        return oNewObj;
    }

    /**
     * Set a JSON to be stored on the consent-document.
     *
     * @param object The object which should passed to the consent-document.
     */
    this.setConsentData = function(object) {
        if(typeof object !== "undefined" && Object.prototype.toString.call(object) === '[object Object]') {
            this.consentData = object;
        } else {
            window.jentis.helper.logError("J-Consent Engine", "setConsentData > Passed Parameter is not a JSON!");
        }
    }

    /**
     * Returns a JSON containing the custom data stored on a consent-document.
     *
     * @returns {*|{}} The object stored on the consent-document.
     */
    this.getConsentData = function() {
        return this.consentData;
    }

    /**
     * FALLBACK FUNCTIONS TO HELPER
     */

    this.addEventListener = function (sName, cb) {
        window.jentis.helper.addEventListener(sName, cb, true);
    }

    /**
     * Function reference for the jentisApp
     */
    this.resetConsent = function() {
        this.aStorage = {};
        this.store2LocalStorage(null); // Default value must be null to set everything correctly in init()

        window.jentis.tracker.log("J-Consent Engine", "Consent has been reset.");

        window.jentis.tracker.resetCachedTrackingData();
    }

};

//We have to set the init event external because the object jentis.consent.engine must be created at this time so
//others can access the object within the event call.
window.jentis.consent.engine.init();
