window.jentis = window.jentis || {};
window.jentis.consent = window.jentis.consent || {};

window.jentis.consent.consentBar = new function () {

    this.init = function () {
        //Prüfen ob wir in einem IFrame sind. Falls ja - nichts ausspielen aber die Tools steuern.
        if (window.self !== window.top) {
            return true;
        }

        this.bEngineReady = false;
        this.bDocumentReady = false;
        this.bInitLateBarShow = false;
        this.oConsentListenerEngine = document;

        //CSS Class Name Prefix
        this.styles = "jentis-consent-";
        this.init_eventlistener();

    }

    this.init_eventlistener = function () {
        if (
            typeof window.jentis.helper !== "undefined" &&
            typeof window.jentis.consent.engine !== "undefined"
        ) {
            this.oConsentListenerEngine = window.jentis.consent.engine;
        }

        //Trigger von Außen um die Oberfläche anzuzeigen
        (function (oMe) {
            oMe.oConsentListenerEngine.addEventListener('jentis.consent.engine.init', function (e) {
                oMe.init_getData.call(oMe);

                oMe.templateConfig = window.jentis.consent.engine.getTemplateConfig();

                oMe.bEngineReady = true;
                oMe.startGui();

            }, false);

            if (document.readyState !== "loading") {
                //The document is allready loaded, so we can start immidiatly
                oMe.bDocumentReady = true;
                oMe.startGui();
            } else {
                //The document is not allready loaded, so we have to listen to DOMContentLoaded
                oMe.oConsentListenerEngine.addEventListener("DOMContentLoaded", function () {
                    oMe.bDocumentReady = true;
                    oMe.startGui();
                });
            }


            oMe.oConsentListenerEngine.addEventListener('jentis.consent.engine.show-bar', function (e) {
                if (document.getElementsByClassName(oMe.styles + "bar").length > 0) {
                    oMe.showBottomBar();
                } else {
                    oMe.bInitLateBarShow = true;
                }

            }, false);

            oMe.oConsentListenerEngine.addEventListener('jentis.consent.engine.user-show-settings', function (e) {
                oMe.showModal();

            }, false);


        })(this);
    }

    this.init_getData = function () {
        var vendorData = window.jentis.consent.engine.getVendorFullData();
        newVendorData = {};
        for (var sVendorId in vendorData) {
            var oLoopVendorData = vendorData[sVendorId];

            var sJustificationId = oLoopVendorData.justification.id
            var sCategoryId = oLoopVendorData.category.id

            newVendorData[sJustificationId] = newVendorData[sJustificationId] || {};
            newVendorData[sJustificationId]["categories"] = newVendorData[sJustificationId]["categories"] || {};
            newVendorData[sJustificationId]["justificationname"] = oLoopVendorData.justification.name;
            newVendorData[sJustificationId]["justificationid"] = oLoopVendorData.justification.id;
            newVendorData[sJustificationId]["categories"][sCategoryId] = newVendorData[sJustificationId]["categories"][sCategoryId] || {};
            newVendorData[sJustificationId]["categories"][sCategoryId]["vendors"] = newVendorData[sJustificationId]["categories"][sCategoryId]["vendors"] || {};
            newVendorData[sJustificationId]["categories"][sCategoryId]["categoryname"] = oLoopVendorData.category.name
            newVendorData[sJustificationId]["categories"][sCategoryId]["categoryid"] = oLoopVendorData.category.id;

            newVendorData[sJustificationId]["categories"][sCategoryId]["vendors"][sVendorId] = oLoopVendorData;
        }


        if (typeof window.jentis.consent.config === "undefined") {
            console.log("JENTIS Consent Bar: No JENTIS Consent Engine Config found");
            return true;
        }


        this.vendorData = newVendorData;
        this.consentId = window.jentis.consent.engine.getConsentId();
        this.lastUpdate = window.jentis.consent.engine.getLastUpdateTime();


    }

    this.dom_eventlistener = function () {
        //Trigger von Außen um die Oberfläche anzuzeigen
        (function (oMe) {
            document.getElementsByClassName(oMe.styles + "button-save")[0].addEventListener('click', function () {
                oMe.save.call(oMe);
            }, false);

            document.getElementsByClassName(oMe.styles + "button-agree")[0].addEventListener('click', function () {
                oMe.allagree.call(oMe);
            }, false);

            document.getElementsByClassName(oMe.styles + "button-agree")[1].addEventListener('click', function () {
                oMe.allagree.call(oMe);
            }, false);

            document.getElementsByClassName(oMe.styles + "button-setting")[0].addEventListener('click', function () {
                oMe.showModal.call(oMe);
            }, false);

            document.getElementsByClassName(oMe.styles + "button-deny")[0].addEventListener('click', function () {
                oMe.alldeny.call(oMe);
            }, false);

            document.getElementsByClassName(oMe.styles + "button_close")[0].addEventListener('click', function () {
                oMe.closeModal.call(oMe);
            }, false);

            document.getElementsByClassName(oMe.styles + "vendor-info-button-back")[0].addEventListener('click', function () {
                oMe.showCategoryList.call(oMe);
            }, false);


            var aCategoryLabels = document.querySelectorAll(".jentis-consent-category_wrapper>li>label");
            for (var i = 0; i < aCategoryLabels.length; i++) {
                aCategoryLabels[i].addEventListener('click', function () {
                    oMe.openCategory.call(oMe, this);
                });
            }

            var aCategoryCheckboxes = document.querySelectorAll(".jentis-consent-category_wrapper>li>input");
            for (var i = 0; i < aCategoryCheckboxes.length; i++) {
                aCategoryCheckboxes[i].addEventListener('click', function () {
                    oMe.clickCategory.call(oMe, this);
                });
            }

            var aCategoryVendors = document.querySelectorAll(".jentis-consent-category_wrapper>li>ul>li>label");
            for (var i = 0; i < aCategoryVendors.length; i++) {
                aCategoryVendors[i].addEventListener('click', function () {
                    oMe.clickCategoryVendor.call(oMe, this);
                });
            }

        })(this);
    }

    this.openCategory = function (oLabel) {
        var oLi = oLabel.parentNode;
        var oUlChecboxWrapper = oLi.querySelectorAll("ul.jentis-consent-category-vendors-checkbox-wrapper")[0]
        if (oUlChecboxWrapper.style.display === "block") {
            oUlChecboxWrapper.style.display = "none";
        } else {
            oUlChecboxWrapper.style.display = "block";
        }
    }

    this.clickCategory = function (oCheckbox) {
        var oLi = oCheckbox.parentNode;
        var bCategoryChecked = oCheckbox.checked

        var aVendorCheckboxes = oLi.querySelectorAll("ul input");

        for (var i = 0; i < aVendorCheckboxes.length; i++) {
            aVendorCheckboxes[i].checked = bCategoryChecked;
        }
    }

    this.clickCategoryVendor = function (oLabel) {
        var oLi = oLabel.parentNode;
        var sVendorId = oLi.getAttribute("data-vendorid");

        document.getElementsByClassName("jentis-consent-category_wrapper")[0].style.display = "none";
        document.getElementById("jentis-consent-vendor-info-" + sVendorId).style.display = "block";
        document.getElementsByClassName("jentis-consent-vendor_info")[0].style.display = "block";

    }

    this.showCategoryList = function (oButton) {
        //Hide all Vendor Infos Li again.
        var aVendorInfosLis = document.querySelectorAll(".jentis-consent-vendor_info-content");
        for (var i = 0; i < aVendorInfosLis.length; i++) {
            aVendorInfosLis[i].style.display = "none";
        }

        document.getElementsByClassName("jentis-consent-vendor_info")[0].style.display = "none";
        document.getElementsByClassName("jentis-consent-category_wrapper")[0].style.display = "block";


    }


    this.showModal = function () {
        document.getElementsByClassName(this.styles + "modal")[0].style.display = 'block';
        //scroll to top:
        window.scrollTo(0, 0);
    }

    this.closeModal = function () {
        document.getElementsByClassName(this.styles + "modal")[0].style.display = 'none';
    }

    this.closeBottomBar = function () {
        document.getElementsByClassName(this.styles + "bar")[0].style.display = 'none';
    }

    this.showBottomBar = function () {
        document.getElementsByClassName(this.styles + "bar")[0].style.display = 'flex';
    }

    this.alldeny = function () {
        window.jentis.consent.engine.DenyAll();
        this.refreshGui();
        this.closeBottomBar();
        this.closeModal();
    }

    this.refreshGui = function () {
        this.init_getData();
        this.getHtml();
        this.dom_eventlistener();
    }

    this.allagree = function () {
        window.jentis.consent.engine.AcceptAll();
        this.refreshGui();
        this.closeBottomBar();
        this.closeModal();
    }

    this.save = function () {
        var aVendorCheckboxes = document.querySelectorAll(".jentis-consent-category-vendors-checkbox-wrapper .jentis-consent-checkbox");
        var aVendors = {};
        for (var i = 0; i < aVendorCheckboxes.length; i++) {
            var sId = aVendorCheckboxes[i].value;
            aVendors[sId] = aVendorCheckboxes[i].checked;
        }

        window.jentis.consent.engine.setNewVendorConsents(aVendors);
        this.closeBottomBar();
        this.closeModal();
    }


    //*****************************
    //*****************************
    //DOM FUNCTIONS
    //*****************************
    //*****************************
    this.decodeHTML = function (html) {
        var txt = document.createElement("textarea");
        txt.innerHTML = html;
        return txt.value;
    }

    this.startGui = function () {

        if (this.bEngineReady === true && this.bDocumentReady === true) {
            this.getHtml();
            this.dom_eventlistener();

            if (this.bInitLateBarShow === true) {
                this.showBottomBar();
            }
        }
    }

    this.createDom = function (sClassName, oParentDom, sElement, sId) {
        if (typeof sElement === "undefined") {
            sElement = "div";
        }
        oDomElement = document.createElement(sElement);

        if (typeof sId !== "undefined") {
            oDomElement.setAttribute("id", this.styles + sId);
        }

        if (typeof sClassName !== "undefined") {
            this.setDomClass(oDomElement, sClassName);
        }

        if (typeof oParentDom !== "undefined") {
            oParentDom.appendChild(oDomElement);
        }

        return oDomElement;
    }

    this.setDomClass = function (oDomElement, sClassName) {
        oDomElement.setAttribute("class", this.styles + sClassName);
    }

    this.createCheckbox = function (oParent, sId, bSelected, sLabel, bDisabled) {

        var oDomCheckboxLi = this.createDom("checkbox-wrapper", oParent, "li", "checkbox-wrapper-" + sId);
        oDomCheckboxLi.setAttribute("data-vendorid", sId);

        var oDomCheckbox = this.createDom("checkbox", oDomCheckboxLi, "input", "checkbox-" + sId);
        oDomCheckbox.setAttribute("name", this.styles + "checkbox-" + sId);
        oDomCheckbox.setAttribute("value", sId);
        oDomCheckbox.setAttribute("type", "checkbox");
        if (bSelected === true) {
            oDomCheckbox.setAttribute("checked", "checked");
        }
        if (bDisabled === true) {
            oDomCheckbox.setAttribute("disabled", "true");
        }

        var oDomLabel = this.createDom("label", oDomCheckboxLi, "label", "label-" + sId);
        //oDomLabel.setAttribute("for",this.styles+"checkbox-"+sId);
        oDomLabel.innerHTML = sLabel;

        return oDomCheckboxLi;
    }


    //*****************************
    //*****************************
    //HTML OUTPUT
    //*****************************
    //*****************************

    this.getHtml = function () {

        var oDomModal = this.getHtmlModal();
        var oDomConsentBar = this.getHtmlConsentbar();
        this.appendCSS();

        //Outer Div noch nicht vorhanden, daher jetzt erstellen und einhängen.
        oOuterDiv = this.createDom("main", document.getElementsByTagName("body")[0], "div", "main");
        oOuterDiv.appendChild(oDomModal)
        oOuterDiv.appendChild(oDomConsentBar);

        return true;
    }

    this.appendCSS = function () {
        var css = "";

        // 1st - Layer

        // jentis-consent-main
        if (typeof this.templateConfig.fontFamily !== "undefined" && this.templateConfig.fontFamily !== "") {
            css += ".jentis-consent-main { font-family: " + this.templateConfig.fontFamily + "; }";
        }

        // jentis-consent-consent-text
        var jentisConsentBar = [];
        if (
            typeof this.templateConfig.buttonPosition1 !== "undefined" &&
            (
                this.templateConfig.buttonPosition1 === "left" || this.templateConfig.buttonPosition1 === "leftBottom"
            )
        ) {
            jentisConsentBar.push("padding-left: 20px");
            jentisConsentBar.push("padding-right: 0px");
        } else {
            jentisConsentBar.push("padding-left: 0px");
            jentisConsentBar.push("padding-right: 20px");
        }

        css += " .jentis-consent-main .jentis-consent-consent-text { " + jentisConsentBar.join(";") + " }";

        // jentis-consent-button_wrapper div
        if (typeof this.templateConfig.buttonRoundCorners !== "undefined" && this.templateConfig.buttonRoundCorners !== "") {
            css += " .jentis-consent-main .jentis-consent-button_wrapper div { border-radius: " + this.templateConfig.buttonRoundCorners + "; }";
        }

        // jentis-consent-bar
        jentisConsentBar = [];
        if (typeof this.templateConfig.backgroundColor1 !== "undefined" && this.templateConfig.backgroundColor1 !== "") {
            jentisConsentBar.push("background-color: " + this.templateConfig.backgroundColor1);
        }

        if (typeof this.templateConfig.topBorderColor1 !== "undefined" && this.templateConfig.topBorderColor1 !== "") {
            jentisConsentBar.push("border-top: 1px solid " + this.templateConfig.topBorderColor1);
        }

        if (jentisConsentBar.length > 0) {
            css += " .jentis-consent-main .jentis-consent-bar { " + jentisConsentBar.join(";") + " }";
        }

        // jentis-consent-button-agree
        jentisConsentBar = [];
        if (typeof this.templateConfig.acceptAllBackgroundColor1 !== "undefined" && this.templateConfig.acceptAllBackgroundColor1 !== "") {
            jentisConsentBar.push("background-color: " + this.templateConfig.acceptAllBackgroundColor1);
        }

        if (typeof this.templateConfig.acceptAllBorderColor1 !== "undefined" && this.templateConfig.acceptAllBorderColor1 !== "") {
            jentisConsentBar.push("border: 1px solid " + this.templateConfig.acceptAllBorderColor1);
        }

        if (typeof this.templateConfig.acceptAllFontColor1 !== "undefined" && this.templateConfig.acceptAllFontColor1 !== "") {
            jentisConsentBar.push("color: " + this.templateConfig.acceptAllFontColor1);
        }

        if (typeof this.templateConfig.showAcceptAllButton1 !== "undefined" && this.templateConfig.showAcceptAllButton1 === "no") {
            jentisConsentBar.push("display: none");
        }

        if (jentisConsentBar.length > 0) {
            css += " .jentis-consent-main .jentis-consent-button_wrapper div.jentis-consent-button-agree.button-prim { " + jentisConsentBar.join(";") + " }";
        }

        // jentis-consent-button-setting
        jentisConsentBar = [];
        if (typeof this.templateConfig.settingsBackgroundColor1 !== "undefined" && this.templateConfig.settingsBackgroundColor1 !== "") {
            jentisConsentBar.push("background-color: " + this.templateConfig.settingsBackgroundColor1);
        }

        if (typeof this.templateConfig.settingsBorderColor1 !== "undefined" && this.templateConfig.settingsBorderColor1 !== "") {
            jentisConsentBar.push("border: 1px solid " + this.templateConfig.settingsBorderColor1);
        }

        if (typeof this.templateConfig.settingsFontColor1 !== "undefined" && this.templateConfig.settingsFontColor1 !== "") {
            jentisConsentBar.push("color: " + this.templateConfig.settingsFontColor1);
        }

        if (typeof this.templateConfig.showAcceptSettingsButton1 !== "undefined" && this.templateConfig.showAcceptSettingsButton1 === "no") {
            jentisConsentBar.push("display: none");
        }

        if (jentisConsentBar.length > 0) {
            css += " .jentis-consent-main .jentis-consent-button_wrapper div.jentis-consent-button-setting { " + jentisConsentBar.join(";") + " }";
        }

        // jentis-consent-button-deny
        jentisConsentBar = [];
        if (typeof this.templateConfig.rejectBackgroundColor1 !== "undefined" && this.templateConfig.rejectBackgroundColor1 !== "") {
            jentisConsentBar.push("background-color: " + this.templateConfig.rejectBackgroundColor1);
        }

        if (typeof this.templateConfig.rejectBorderColor1 !== "undefined" && this.templateConfig.rejectBorderColor1 !== "") {
            jentisConsentBar.push("border: 1px solid " + this.templateConfig.rejectBorderColor1);
        }

        if (typeof this.templateConfig.rejectFontColor1 !== "undefined" && this.templateConfig.rejectFontColor1 !== "") {
            jentisConsentBar.push("color: " + this.templateConfig.rejectFontColor1);
        }

        if (typeof this.templateConfig.showAcceptRejectButton1 !== "undefined" && this.templateConfig.showAcceptRejectButton1 === "no") {
            jentisConsentBar.push("display: none");
        }

        if (jentisConsentBar.length > 0) {
            css += " .jentis-consent-main .jentis-consent-button_wrapper div.jentis-consent-button-deny { " + jentisConsentBar.join(";") + " }";
        }

        // 2nd - Layer

        // jentis-consent-modal
        jentisConsentBar = [];
        if (typeof this.templateConfig.backgroundColor2 !== "undefined" && this.templateConfig.backgroundColor2 !== "") {
            jentisConsentBar.push("background-color: " + this.templateConfig.backgroundColor2);
        }

        if (typeof this.templateConfig.borderColor2 !== "undefined" && this.templateConfig.borderColor2 !== "") {
            jentisConsentBar.push("border: 1px solid " + this.templateConfig.borderColor2);
        }

        if (jentisConsentBar.length > 0) {
            css += " .jentis-consent-main .jentis-consent-modal { " + jentisConsentBar.join(";") + " }";
        }

        // jentis-consent-button-agree
        jentisConsentBar = [];
        if (typeof this.templateConfig.acceptAllBackgroundColor2 !== "undefined" && this.templateConfig.acceptAllBackgroundColor2 !== "") {
            jentisConsentBar.push("background-color: " + this.templateConfig.acceptAllBackgroundColor2);
        }

        if (typeof this.templateConfig.acceptAllBorderColor2 !== "undefined" && this.templateConfig.acceptAllBorderColor2 !== "") {
            jentisConsentBar.push("border: 1px solid " + this.templateConfig.acceptAllBorderColor2);
        }

        if (typeof this.templateConfig.acceptAllFontColor2 !== "undefined" && this.templateConfig.acceptAllFontColor2 !== "") {
            jentisConsentBar.push("color: " + this.templateConfig.acceptAllFontColor2);
        }

        if (typeof this.templateConfig.showAcceptAllButton2 !== "undefined" && this.templateConfig.showAcceptAllButton2 === "no") {
            jentisConsentBar.push("display: none");
        }

        if (jentisConsentBar.length > 0) {
            css += " .jentis-consent-main .jentis-consent-button_wrapper div.jentis-consent-button-agree.button-sec { " + jentisConsentBar.join(";") + " }";
        }

        // jentis-consent-button-save
        jentisConsentBar = [];
        if (typeof this.templateConfig.saveBackgroundColor2 !== "undefined" && this.templateConfig.saveBackgroundColor2 !== "") {
            jentisConsentBar.push("background-color: " + this.templateConfig.saveBackgroundColor2);
        }

        if (typeof this.templateConfig.saveBorderColor2 !== "undefined" && this.templateConfig.saveBorderColor2 !== "") {
            jentisConsentBar.push("border: 1px solid " + this.templateConfig.saveBorderColor2);
        }

        if (typeof this.templateConfig.saveFontColor2 !== "undefined" && this.templateConfig.saveFontColor2 !== "") {
            jentisConsentBar.push("color: " + this.templateConfig.saveFontColor2);
        }

        if (typeof this.templateConfig.showSaveButton2 !== "undefined" && this.templateConfig.showSaveButton2 === "no") {
            jentisConsentBar.push("display: none");
        }

        if (jentisConsentBar.length > 0) {
            css += " .jentis-consent-main .jentis-consent-button_wrapper div.jentis-consent-button-save { " + jentisConsentBar.join(";") + " }";
        }

        if (typeof this.templateConfig.css !== "undefined" && this.templateConfig.css !== "") {
            css += this.templateConfig.css;
        }

        var oStyle = this.createDom(undefined, document.head || document.getElementsByTagName('head')[0], "style");
        oStyle.type = "text/css";
        if (oStyle.styleSheet) {
            // This is required for IE8 and below.
            oStyle.styleSheet.cssText = css;
        } else {
            oStyle.appendChild(document.createTextNode(css));
        }
    }

    this.getHtmlCategoryBox = function (oCategoryData, bLegitimateInt) {

        var oDomCategoryCheckbox = this.createCheckbox(undefined, bLegitimateInt ? "legitimate-interest" : oCategoryData.categoryid, bLegitimateInt, bLegitimateInt ? "Legitimate interest" : oCategoryData.categoryname, bLegitimateInt)
        var oDomCategoryVendors = this.createDom("category-vendors-checkbox-wrapper", oDomCategoryCheckbox, "ul");

        var bCheckCategory = true;
        for (var sVendorId in oCategoryData.vendors) {
            var oDomVendorCheckbox = this.createCheckbox(oDomCategoryVendors, sVendorId, bLegitimateInt ? true : oCategoryData.vendors[sVendorId].status, oCategoryData.vendors[sVendorId].vendor.name, bLegitimateInt);
            if (oCategoryData.vendors[sVendorId].status === false) {
                bCheckCategory = false;
            }
        }

        if (bCheckCategory === true) {
            oDomCategoryCheckbox.getElementsByTagName("input")[0].checked = true;
        }

        return oDomCategoryCheckbox

    }

    this.getHtmlModal = function () {

        //First delete an old modal
        var oOldModal = document.getElementsByClassName(this.styles + "modal");
        if (oOldModal.length > 0) {
            oOldModal[0].parentNode.removeChild(oOldModal[0]);
        }

        var oDomDivModal = this.createDom("modal");

        //Set the Close Button.
        var oDomDivCloseButtonWrapper = this.createDom("button_close_wrapper", oDomDivModal)
        var oDomDivCloseButton = this.createDom("button_close", oDomDivCloseButtonWrapper)
        oDomDivCloseButton.innerHTML = "x";


        //Set the Category Checkboxes and the vendor Info Boxes
        var oDomDivatCegories = this.createDom("category_wrapper", oDomDivModal, "ul");
        var oDomDivVendorInfos = this.createDom("vendor_info", oDomDivModal, "div");

        var oDomDivVendorInfoContent = this.createDom("vendor-info-button-back", oDomDivVendorInfos, "div");
        oDomDivVendorInfoContent.innerHTML = "back to list";


        var oDomDivVendorInfosUl = this.createDom("vendor_info_ul", oDomDivVendorInfos, "ul");

        if (typeof this.vendorData["consent"] != "undefined") {
            for (var sCategoryId in this.vendorData["consent"]["categories"]) {
                var oDomCategoryBox = this.getHtmlCategoryBox(this.vendorData["consent"]["categories"][sCategoryId], false);
                oDomDivatCegories.appendChild(oDomCategoryBox);

                var aVendors = this.vendorData["consent"]["categories"][sCategoryId]["vendors"];

                for (sVendorId in aVendors) {

                    var oVendor = aVendors[sVendorId];

                    var oDomDivVendorInfoLi = this.createDom("vendor_info-content", oDomDivVendorInfosUl, "li", "vendor-info-" + sVendorId);

                    var oDomDivVendorInfoContentLEFT = this.createDom("vendor-info-left", oDomDivVendorInfoLi, "div");


                    var oDomDivVendorInfoContent = this.createDom("vendor-info-name", oDomDivVendorInfoContentLEFT, "div");
                    oDomDivVendorInfoContent.innerHTML = oVendor["vendor"]["name"];

                    var oDomDivVendorInfoContent = this.createDom("vendor-info-street", oDomDivVendorInfoContentLEFT, "div");
                    oDomDivVendorInfoContent.innerHTML = oVendor["vendor"]["street"];

                    var oDomDivVendorInfoContent = this.createDom("vendor-info-zip", oDomDivVendorInfoContentLEFT, "div");
                    oDomDivVendorInfoContent.innerHTML = oVendor["vendor"]["zip"];

                    var oDomDivVendorInfoContent = this.createDom("vendor-info-country", oDomDivVendorInfoContentLEFT, "div");
                    oDomDivVendorInfoContent.innerHTML = oVendor["vendor"]["country"]["name"];

                    var oDomDivVendorInfoContent = this.createDom("vendor-info-category", oDomDivVendorInfoContentLEFT, "div");
                    oDomDivVendorInfoContent.innerHTML = "Category: " + oVendor["category"]["name"];

                    var oDomDivVendorInfoContent = this.createDom("vendor-info-justification", oDomDivVendorInfoContentLEFT, "div");
                    oDomDivVendorInfoContent.innerHTML = "Justification: " + oVendor["justification"]["name"];

                    var oDomDivVendorInfoContentRIGHT = this.createDom("vendor-info-right", oDomDivVendorInfoLi, "div");

                    var oDomDivVendorInfoContent = this.createDom("vendor-info-description", oDomDivVendorInfoContentRIGHT, "div");
                    oDomDivVendorInfoContent.innerHTML = this.decodeHTML(oVendor["description"]);

                }
            }
        }

        if (typeof this.vendorData["legitimate-interest"] != "undefined") {
            for (var sLegitimateId in this.vendorData["legitimate-interest"]["categories"]) {
                var oDomCategoryBox = this.getHtmlCategoryBox(this.vendorData["legitimate-interest"]["categories"][sLegitimateId], true);
                oDomDivatCegories.appendChild(oDomCategoryBox);

                var aVendors = this.vendorData["legitimate-interest"]["categories"][sLegitimateId]["vendors"];

                for (sVendorId in aVendors) {

                    var oVendor = aVendors[sVendorId];

                    var oDomDivVendorInfoLi = this.createDom("vendor_info-content", oDomDivVendorInfosUl, "li", "vendor-info-" + sVendorId);

                    var oDomDivVendorInfoContentLEFT = this.createDom("vendor-info-left", oDomDivVendorInfoLi, "div");


                    var oDomDivVendorInfoContent = this.createDom("vendor-info-name", oDomDivVendorInfoContentLEFT, "div");
                    oDomDivVendorInfoContent.innerHTML = oVendor["vendor"]["name"];

                    var oDomDivVendorInfoContent = this.createDom("vendor-info-street", oDomDivVendorInfoContentLEFT, "div");
                    oDomDivVendorInfoContent.innerHTML = oVendor["vendor"]["street"];

                    var oDomDivVendorInfoContent = this.createDom("vendor-info-zip", oDomDivVendorInfoContentLEFT, "div");
                    oDomDivVendorInfoContent.innerHTML = oVendor["vendor"]["zip"];

                    var oDomDivVendorInfoContent = this.createDom("vendor-info-country", oDomDivVendorInfoContentLEFT, "div");
                    oDomDivVendorInfoContent.innerHTML = oVendor["vendor"]["country"]["name"];

                    var oDomDivVendorInfoContent = this.createDom("vendor-info-category", oDomDivVendorInfoContentLEFT, "div");
                    oDomDivVendorInfoContent.innerHTML = "Category: " + oVendor["category"]["name"];

                    var oDomDivVendorInfoContent = this.createDom("vendor-info-justification", oDomDivVendorInfoContentLEFT, "div");
                    oDomDivVendorInfoContent.innerHTML = "Justification: " + oVendor["justification"]["name"];

                    var oDomDivVendorInfoContentRIGHT = this.createDom("vendor-info-right", oDomDivVendorInfoLi, "div");

                    var oDomDivVendorInfoContent = this.createDom("vendor-info-description", oDomDivVendorInfoContentRIGHT, "div");
                    oDomDivVendorInfoContent.innerHTML = this.decodeHTML(oVendor["description"]);

                }
            }
        }

        //Set the Contact Info Box.
        var oDomDivContact = this.createDom("contact_wrapper", oDomDivModal)
        oDomDivContact.innerHTML = this.templateConfig.contact;

        //Set the Button Box.
        var oDomDivButtons = this.createDom("button_wrapper", oDomDivModal)


        var oDomButtonSave = this.createDom("button-save button-prim", oDomDivButtons);
        oDomButtonSave.innerHTML = this.templateConfig.textOfSaveButton2;


        var oDomButtonAgree = this.createDom("button-agree button-sec", oDomDivButtons);
        oDomButtonAgree.innerHTML = this.templateConfig.textOfAcceptAllButton2;


        return oDomDivModal;
    }


    this.getHtmlConsentbar = function () {

        var oDomDivBar = this.createDom("bar");

        if (
            typeof this.templateConfig.buttonPosition1 !== "undefined" &&
            (
                this.templateConfig.buttonPosition1 === "left" || this.templateConfig.buttonPosition1 === "leftBottom"
            )
        ) {
            this.appendLayer1ButtonWrapper(oDomDivBar);
            this.appendLayer1TextWrapper(oDomDivBar);
        } else {
            this.appendLayer1TextWrapper(oDomDivBar);
            this.appendLayer1ButtonWrapper(oDomDivBar);
        }

        return oDomDivBar;

    }

    this.appendLayer1TextWrapper = function (oDomDivBar) {
        var oDomDivTextWrapper = this.createDom("consent-text", oDomDivBar)

        var oDomDivTitleWrapper = this.createDom("consent-title", oDomDivTextWrapper);
        oDomDivTitleWrapper.innerHTML = this.templateConfig.textOfTitle1;

        //Set the Text Box.
        var oDomDivText = this.createDom("consent-text-inner", oDomDivTextWrapper)
        var sConsentText = this.templateConfig.textOfDescription1;
        var sCategories = "";
        if (this.vendorData.length > 0) {
            for (var sCategorieId in this.vendorData["consent"]["categories"]) {
                sCategories += this.vendorData["consent"]["categories"][sCategorieId].categoryname + ", ";
            }
        }

        sCategories = sCategories.substr(0, sCategories.length - 2);
        sConsentText = sConsentText.replace("{{purposes}}", sCategories);
        oDomDivText.innerHTML = sConsentText;

        //Set the Link Box.
        if (typeof this.templateConfig.importantLinks !== "undefined") {
            var oDomDivLinks = this.createDom("consent-links", oDomDivTextWrapper);
            for (var sLinkName in this.templateConfig.importantLinks) {
                var oDomALink = this.createDom(undefined, oDomDivLinks, "a");
                oDomALink.setAttribute("href", this.templateConfig.importantLinks[sLinkName]);
                oDomALink.innerHTML = sLinkName;
            }
        }
    }

    this.appendLayer1ButtonWrapper = function (oDomDivBar) {
        //Set the Button Box.
        var oDomDivButtons = this.createDom("button_wrapper", oDomDivBar)


        var oDomButtonAgree = this.createDom("button-agree button-prim", oDomDivButtons);
        oDomButtonAgree.innerHTML = this.templateConfig.textOfAcceptAllButton1;


        var oDomButtonSettings = this.createDom("button-setting button-sec", oDomDivButtons);
        oDomButtonSettings.innerHTML = this.templateConfig.textOfSettingButton1;


        var oDomButtonDeny = this.createDom("button-deny button-sec", oDomDivButtons);
        oDomButtonDeny.innerHTML = this.templateConfig.textOfRejectButton1;
    }

    this.init();
}
