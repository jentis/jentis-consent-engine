window.jentis = window.jentis || {};
window.jentis.consent = window.jentis.consent || {};
window.jentis.consent.config = {
    "template": {
        "config": {
            "fontFamily": "Verdana",
            "buttonRoundCorners": "0px",
            "textOfTitle1": "TEXT OF FIRST LAYER",
            "textOfDescription1": "DESCRIPTION ON THE FIRST LAYER",
            "backgroundColor1": "#1d8cc3",
            "topBorderColor1": "#ffe01a",
            "buttonPosition1": "right",
            "showAcceptAllButton1": "yes",
            "textOfAcceptAllButton1": "ACCEPT ALL BUTTON",
            "acceptAllBackgroundColor1": "#ac6262",
            "acceptAllFontColor1": "#e9ff42",
            "acceptAllBorderColor1": "#a8d5ff",
            "showAcceptSettingsButton1": "yes",
            "textOfSettingButton1": "SETTINGS BUTTON",
            "settingsBackgroundColor1": "#de4f4f",
            "settingsFontColor1": "#def97b",
            "settingsBorderColor1": "#9eb4f5",
            "showAcceptRejectButton1": "yes",
            "textOfRejectButton1": "REJECT BUTTON",
            "rejectBackgroundColor1": "#ff2929",
            "rejectFontColor1": "#ffd84d",
            "rejectBorderColor1": "#85a3ff",
            "backgroundColor2": "#f499d8",
            "borderColor2": "#fcff52",
            "showAcceptAllButton2": "yes",
            "textOfAcceptAllButton2": "ACCEPPT ALL SECOND",
            "acceptAllBackgroundColor2": "#e10e0e",
            "acceptAllFontColor2": "#3ea2bb",
            "acceptAllBorderColor2": "#eae553",
            "showSaveButton2": "yes",
            "textOfSaveButton2": "SAVE BUTTON",
            "saveBackgroundColor2": "#f55656",
            "saveFontColor2": "#496397",
            "saveBorderColor2": "#9a8c2d",
            "css": ".kletzi {\r\nwidth: 10px;\r\n}"
        }
    },
    "vendors": {
        "googleanalytics": {
            "vendor": {
                "id": "googleanalytics",
                "name": "Google Analytics",
                "street": "Google Building Gordon House, 4 Barrow St",
                "zip": "D04 E5W5",
                "country": {
                    "iso": "IR",
                    "name": "Iran, Islamic Republic of"
                }
            },
            "category": {
                "id": "necessary",
                "name": "Necessary"
            },
            "purposes": [],
            "justification": {
                "id": "consent",
                "name": "Consent"
            },
            "description": "<p>A tracking tool to analyze the website traffic and performance. It creates insights from the website user's data.</p>",
            "no_consent_mode": false,
            "status": true
        },
        "facebook": {
            "vendor": {
                "id": "facebook",
                "name": "Facebook",
                "street": "Menlo Park, California",
                "zip": "94025",
                "country": {
                    "iso": "US",
                    "name": "United States"
                }
            },
            "category": {
                "id": "marketing",
                "name": "Marketing"
            },
            "purposes": [],
            "justification": {
                "id": "consent",
                "name": "Consent"
            },
            "description": "&lt;p&gt;Analyzes the user behaviour on the website. This allows to measure and optimize the effectiveness of ads as well as improved user retargeting.&lt;/p&gt;",
            "no_consent_mode": false,
            "status": true
        },
        "adwords": {
            "vendor": {
                "id": "adwords",
                "name": "Google Ads",
                "street": "Google Building Gordon House, 4 Barrow St",
                "zip": "D04 E5W5",
                "country": {
                    "iso": "IR",
                    "name": "Iran, Islamic Republic of"
                }
            },
            "category": {
                "id": "statistic",
                "name": "Statistic"
            },
            "purposes": [
                {
                    "id": "store-device",
                    "name": "Store and/or access information on a device"
                },
                {
                    "id": "create-ads",
                    "name": "Create a personalised ads profile"
                },
                {
                    "id": "measure-ads",
                    "name": "Measure ad performance"
                }
            ],
            "justification": {
                "id": "consent",
                "name": "Consent"
            },
            "description": "Online advertising platform. Advertisers bid to display brief ads, service offerings, product listings or videos to users. This ads can be placed both in search engines and for example mobile apps.",
            "no_consent_mode": false,
            "status": true
        },
        "rtb": {
            "vendor": {
                "id": "rtb",
                "name": "RTB House",
                "street": "Złota Street 61/101, Warsaw",
                "zip": "00-819",
                "country": {
                    "iso": "PL",
                    "name": "Poland"
                }
            },
            "category": {
                "id": "marketing",
                "name": "Marketing"
            },
            "purposes": [],
            "justification": {
                "id": "consent",
                "name": "Consent"
            },
            "description": "Provides personalized marketing campaigns based on deep learning algorithms, including customer segmentation and monitoring.",
            "no_consent_mode": false,
            "status": true
        },
        "adformdmp": {
            "vendor": {
                "id": "adformdmp",
                "name": "AdForm AdServer",
                "street": "",
                "zip": "",
                "country": {
                    "iso": "US",
                    "name": "United States"
                }
            },
            "category": {
                "id": "marketing",
                "name": "Marketing"
            },
            "purposes": [
                {
                    "id": "store-device",
                    "name": "Store and/or access information on a device"
                },
                {
                    "id": "create-ads",
                    "name": "Create a personalised ads profile"
                },
                {
                    "id": "select-pers-ads",
                    "name": "Select personalised ads"
                },
                {
                    "id": "security",
                    "name": "Ensure security, prevent fraud, and debug"
                },
                {
                    "id": "tech-deliver",
                    "name": "Technically deliver ads or content"
                },
                {
                    "id": "offline-sources",
                    "name": "Match and combine offline data sources"
                },
                {
                    "id": "link-devices",
                    "name": "Link different devices"
                },
                {
                    "id": "receive-device",
                    "name": "Receive and use automatically-sent device characteristics for identification"
                }
            ],
            "justification": {
                "id": "consent",
                "name": "Consent"
            },
            "description": "Advertising technology platform to manage the whole campaign life cycle. It combines media planning, reporting and  adserving.",
            "no_consent_mode": false,
            "status": true
        }
    },
    "i18n": {
        "basic-lng": "en"
    }
}


/*
window.jentis = window.jentis || {};
window.jentis.consent = window.jentis.consent || {};
window.jentis.consent.config = {
	
	timeoutBarShow : 3600000,	
	backward : {
		vendorduplicate : "pixel"
	},
	bModeStartInitTrackOnJustificationOther : true,
	template : {
		config : {
			consentText 	: "<h4>Dataprotection Settings</h4>Wir verwenden Cookies und &auml;hnliche Technologien f&uuml;r folgende Zwecke: {{purposes}}. Mit Klick auf \"Zustimmen\" willigen Sie der Verwendung dieser Cookies ein. Mit \"Ablehnen\" lehnen Sie diese Cookies ab. Die gesamten Cookie-Einstellungen k&ouml;nnen Sie in den Cookie-Einstellungen verwalten.",
			contact			: "JENTIS GmbH<br>Sch&ouml;nbrunnerstra�e 231, 1120 Wien<br>Austria<br>+43-1-2234 00 33<br>dataprotection@jentis.com",
			buttonAgree		: "Alle akzeptieren",
			buttonDeny		: "Ablehnen",
			buttonSetting	: "Einstellungen",
			buttonSave		: "Speichern",
			importantLinks	: {
				"Impressum"		: "/impressum",
				"Datenschutz"	: "/datenschutz"
			}		
		}
	},
	vendors : 
	{
		"ga" : {

			"vendor" : {
				"id"      : "ga",
				"name"    : "Google Analytics",
				"street"  : "Google Street 1",
				"zip"     : "114011",
				"country" : {
					"iso"   : "us",
					"name"  : "United States of America"
				}
			},
			"purpose" : {
				"id"    : "stat",
				"name"  : "Statistik"
			},
			"justification" : {
				"id"    : "other",
				"name"  : "consent"
			},
			"deniable"	: false,
			"description" : "bla bla bla bla bla"
		},
		"fb"  : {
			"vendor" : {
				"id"      : "fb",
				"name"    : "Facebook",
				"street"  : "FB Street 1",
				"zip"     : "114011",
				"country" : {
					"iso"   : "us",
					"name"  : "United States of America"
				}
			},
			"purpose" : {
				"id"    : "mark",
				"name"  : "Marketing"
			},
			"justification" : {
				"id"    : "legal",
				"name"  : "Legal Fullfillment"
			},
			"deniable"	: true,
			"description" : "bla bla bla bla bla"
			
		},
		"adw"  : {
			"vendor" : {
				"id"      : "adw",
				"name"    : "Adwords",
				"street"  : "FB Street 1",
				"zip"     : "114011",
				"country" : {
					"iso"   : "us",
					"name"  : "United States of America"
				}
			},
			"purpose" : {
				"id"    : "mark",
				"name"  : "Marketing"
			},
			"justification" : {
				"id"    : "consent",
				"name"  : "Legal Fullfillment"
			},
			"deniable"	: true,
			"description" : "bla bla bla bla bla"
			
		}
		
	}

};
*/